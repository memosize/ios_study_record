# UI 知识点总结
## 毛玻璃效果

    UIImageView * imageView = [[UIImageView alloc] init];
    imageView.frame = self.view.bounds;
    // 设置背景颜色
    imageView.backgroundColor = [UIColor redColor];
    imageView.image = [UIImage imageNamed:@"22.jpg"];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imageView];
    UIToolbar * toolbar = [[UIToolbar alloc] init];
    toolbar.frame = self.view.bounds;
	toolbar.barStyle = UIBarStyleBlack;
    [imageView addSubview:toolbar];
## UILable 属性介绍
linebreak 决定在什么地方省略  
truncate head 头部  
truncate middle 中间  
truncate trail 尾部  
wordbreak 以完整的单词结尾  
charcterbreak 可以不是完整的单词结尾  
clip 多余的直接裁剪  
label 自动换行 ：设置numberoflines = 0;(前提是label高度达到)  
# UIImageVIew  属性介绍
UIViewContentModeScaleToFill // 填充View  
UIViewContentModeScaleAspectToFit 自适应比例大小显示 一般用这个  
UIViewContentModeScaleAspectFill 原始大小显示  
带有scale 有可能会被拉伸或者压缩  
带有aspect 只会失真不会变形  
[contentMode详解](http://www.mamicode.com/info-detail-908979.html "contentMode详解")
# UIButton 
## button 状态
normoal 正常状态   
heighted 高亮 (点击未松开的时候)   
selected 选中   
## button的image
image 内容图片 可以动态调整   
background 背景图片   
## 总结
button 是由一个ImageView 和 titleLable 构成的
custom类型的 需要自己设置button的文字，图片。如果只有问题或者图片，默认居中，但是二者都有图片在左边，文字在右边，当然也可以设置二者的对齐方式。   
# 懒加载
## 作用
全局只会加载一次，只会在需要的时候创建，全局都可用使用  
## 用法
重写成员变量get方法   
判断数组是否为nil，为空创建，否则直接获取。   
# 字典转模型
## 优点
如果使用字典，没有智能提示，需要手打，容易出错
```objective-c
- (NSArrar * )dataArr{
	if (_dataArr == nil)
	{
		NSString * dataPath = [NSBundle mainBundle] pathForResource@"shop.plist" ofTypr:nil];//获取路径
self.dataArr = [NSArray arrayWithContentOfFile:dataPath];
// 字典转模型
// 创建临时数组
NSMutableArray * tmpArr =  [NSMutableArray array];
for(NSDictory * dict in self.dataArr)
{
	Shop * shop = [[Shop alloc] init];
	shop.name = dict[@"name"];
	shop.icon = dict[@"icon"];
	[tmpArr addObject:shop];
}
self.dataArr = tmpArr;
	}
	return _dataArr;

}

```
实际开发中应当自定义类方法，实例方法，传入字典生成模型(字典有利于解耦);  
# 自定义控件
## Super init
在子类中调用父控件的方法，是为了不把父控件的方法一起修改！   
要做到所有的子控件的初始化都在内部实现。子控件的frame 可以在 layoutSubViews 中设置   
重写initWithFrame 会默认调用init方法   
# 图片拉伸 
    +(instancetype)resizebleImageWithLocalImageName:(NSString *)loacalImageName
    {
        //创建图片对象
        UIImage * image = [UIImage imageNamed:loacalImageName];
        //获取图片尺寸
        CGFloat imageWidth = image.size.width;
        CGFloat imageHeight = image.size.height;
        return [image stretchableImageWithLeftCapWidth:imageWidth * 0.5 topCapHeight:imageHeight * 0.5];
    }
    
# KVC 
## keypath 和 key
keypath 完全包括key的功能，但是keypanth 可以支持内部点语法。
# UIScrollView
## 内容缩放
### 设置缩放比例
    self.scrollView.maxmumZoomScale=2.0;
    self.scrollView.minmumZoomScale=0.5;
    
    
### 代理方法中返回要缩放的View
    (UIView *)ViewForZoomInScrollView:(UIScrollView *)scrollView
    {
    	retuen ImageView;
    }
# 约束优先级 和 约束动画
设置约束优先级是为了在view的依赖view不在的时候可以依赖其他的View   
苹果希望通过约束得到的frame 修改frame 也应该去修改约束而不是直接去修改frame。   
        self.wConstraint.constant = 50;
    
        [UIView animateWithDuration:1.5 animations:^{
            // 强制刷新
            [self.view layoutIfNeeded];
            _redVIew.backgroundColor = [UIColor blueColor];
        }];
#PCH文件
## 作用
1. 存放一些公用的宏
2. 导入公用的头文件
3. 自定义nslog等

#UIApplication 作用
1. 设置提醒图标
2. 设置联网状态
3. 设置状态栏
4. 打开网页   
##设置提醒图标
``` object-c
UIApplication * app = [UIApplication shareApplication];
UIUsernotificationSetting * setting = [UIUsernotificationSetting settingForTypes:
UINotificationTypeBadge];
[app registerUserNotifacationSettings:setting]
```
## 设置联网状态
app.networkActivityIndicatorVisiable = YES;
## 状态栏默认是由viewcontroller管理的，需要在plist中配置由UIApplication 管理
#多线程
##进程与线程
###线程的串行   
任务一个一个顺序执行，同一时间只能执行一个任务
1. 异步函数➕并发队列：会开启多条线程队列中的任务是并发执行的   
2. 异步函数➕串行队列：会开一条线程，队列中的任务是串行执行的
3. 同步函数➕并发队列：不会开线程，任务是串行执行的
4. 同步函数➕串行队列：不会开线程，任务是串行执行的
5. 异步函数➕主队列：只会在主线程中执行
6. 同步函数➕主队列：死锁（互相等待）
### GCD常用函数
#### delay   

```object-c
dispatch_time_t delayTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0/*延迟执行时间*/ * NSEC_PER_SEC));

dispatch_after(delayTime, dispatch_get_main_queue(), ^{
    [weakSelf delayMethod];
});
```
####栅栏函数   
在进程管理中起到一个栅栏的作用,它等待所有位于barrier函数之前的操作执行完毕后执行,并且在barrier函数执行之后,barrier函数之后的操作才会得到执行,该函数需要同dispatch_queue_create函数生成的concurrent Dispatch Queue队列一起使用   

```object-c
- (void)barrier
{
　　//同dispatch_queue_create函数生成的concurrent Dispatch Queue队列一起使用
    dispatch_queue_t queue = dispatch_queue_create("12312312", DISPATCH_QUEUE_CONCURRENT);
    
    dispatch_async(queue, ^{
        NSLog(@"----1-----%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"----2-----%@", [NSThread currentThread]);
    });
    
    dispatch_barrier_async(queue, ^{
        NSLog(@"----barrier-----%@", [NSThread currentThread]);
    });
    
    dispatch_async(queue, ^{
        NSLog(@"----3-----%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"----4-----%@", [NSThread currentThread]);
    });
}
```   
# RUNLoop
每一条线程都有一条唯一与之对应的runloop，主线程自动创建，子线程需要自己创建
在线程结束的时候销毁。   
获取主线程对应的runloop：[NSRunLoop MainRunLoop]
获取子线程对应的runloop:[NSRunLoop CurrentRunloop]   
CurrentRunloop 是一个懒加载的方法，可以用来直接创建子线程的runloop
runloop 有多种运行模式，但是runloop只能选择一种运行模式。
子线程开启runloop:
```
[NSThred alloc] initwithTarget:self seclector:@seclector(run:) object:nil]   
```   
每一个model里面至少要有一个source 和 timer

